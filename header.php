<!DOCTYPE html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />


<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" type="image/x-icon"> 

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css?v=<?= $time; ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css?v=<?= $time; ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font.css?v=<?= $time; ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/global.css?v=<?= $time; ?>">

<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.9.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=<?= $time; ?>"></script>


<?php 	wp_head();
		$time = time();
 ?>
 
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118894391-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118894391-1');
</script>

</head>

<body>
    
<div id="wrapper">
        
      <header class="main">
          <div class="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a></div>
          <nav class="top">
		  <?php wp_nav_menu( array('menu' => 'MainNav'));  ?>
		  <!--
              <ul>
                  <li <?php if(is_front_page() || is_page('home')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>" class="red" title="Home">Home</a></li>
                  <li <?php if(is_page('about-us')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>/about-us" class="orange" title="About Us">About Us</a></li>
                  <li <?php if(is_category('event-photos')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>/event-photos" class="brown" title="Event & Photos">Event & Photos</a></li>
                  <li <?php if(is_page('enrolment')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>/enrolment" class="green" title="Enrolment">Enrolment</a></li>
                  <li <?php if(is_page('contact')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>/contact" class="blue" title="Contact">Contact</a></li>
				  <li <?php if(is_page('contact')) echo "class='current'"; ?>><a href="<?php bloginfo('url'); ?>/contact" class="blue" title="Contact">Contact</a></li>
              </ul>
			 -->
          </nav>
      </header>
      
      <div id="banner"><img src="<?php echo get_template_directory_uri(); ?>/img/banner-new.jpg">
      </div>
      
      <div id="container">
      	  

    
 