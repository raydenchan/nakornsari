<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<h1>Event & Photos</h1>
    <article class="full">   
	<?php while ( have_posts() ) : the_post(); ?>
    <h2 class="topic"><span class="bee"></span><?php the_title(); ?></h2>
    <?php the_content(); ?>
    <?php endwhile; ?>
    </article>

<?php get_footer(); ?>