<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<h1>Event & Photos</h1>

    <aside class="links">
        <h2 class="topic"><span class="bee"></span>Event List</h2>
        <ul>
        <?php wp_get_archives('type=postbypost&yearly&cat=2'); ?>
        </ul> 
    </aside>

    <article class="imagewrap">   
    <?php while ( have_posts() ) : the_post(); ?>
    <h2 class="topic"><span class="bee"></span><?php the_title(); ?></h2>
    <?php the_content(); ?>
    <?php endwhile; ?>
    </article>
    
    
<?php get_footer(); ?>